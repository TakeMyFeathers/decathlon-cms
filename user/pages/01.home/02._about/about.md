---
title: 'O projekcie'
menu: 'O projekcie'
anchor: about
section_title: 'O projekcie'
paragraph_bold: 'Góry, ludzie, wyzwanie, przygoda. pójdź z nami tam, gdzie jeszcze nie byłeś. sprawdź swoje możliwości i odkryj tatry na nowo!'
paragraph_normal: 'Chcemy zainspirować Cię do górskich wędrówek, dlatego ruszamy na mniej uczęszczane szlaki w Tatrach Słowackich. Śledź nasze wyprawy, które poprowadzą doświadczeni górscy eksperci: '
paragraph_featured: 'Magdalena Derezińska – Osiecka i Krzysztof Starnawski'
carousel: 'Eksperci górscy'
slides:
    -
        name: Krzysztof
        surname: Starnawski
        description: 'Od zawsze czuł potrzebę poznawania świata i szukania przygód, dlatego też od 20 lat eksploruje jaskinie całego świata. W 2016 r. wraz z polsko-czeskim zespołem nurków i badaczy udowodnił, że Hranická Propast jest najgłębszą podwodną jaskinią świata. Jest ratownikiem TOPR, taternikiem, speleologiem. '
        photo:
            'Group 30.png':
                name: 'Group 30.png'
                type: image/png
                size: 260638
                path: 'Group 30.png'
    -
        name: Magdalena
        surname: Derezińska-Osiecka
        description: 'Jej żywioł to połączenie gór i dyscyplin wytrzymałościowych. W butach biegowych, na nartach, czy w rakach pokonała przewyższenia takie jak Pik Lenina (7134m.) czy Pico Orizaba (5700m.), Mt.Aragac (4095m.). Tatry są dla niej jak dom, po którym oprowadza jako przewodnik tatrzański. '
        photo:
            'Group 35.png':
                name: 'Group 35.png'
                type: image/png
                size: 259710
                path: 'Group 35.png'

---

