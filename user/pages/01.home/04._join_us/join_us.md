---
title: 'Zostań decasterem'
menu: 'Zostań decasterem'
anchor: join_us
description: 'Masz apetyt na przygodę? Zainspiruj się naszymi wyprawami i ruszaj w góry. Podziel się swoimi doświadczeniami, pokaż nam swój górski świat i zostań decasterem.'
button_text: 'Zostań decasterem'
card_name: 'Tomek Habdas'
card_type: Decatester
card_description: 'Cześć, nazywam się Tomek i kocham góry!<br>Tej miłości nagromadziło się we mnie tak dużo, że w pewnym momencie postanowiłem ją wydobyć na zewnątrz. Tak powstał projekt W Szczytowej Formie, czyli platforma do dzielenia się pasją, ale przede wszystkim ogromną radością, jaką dają nam góry. Nie bez powodu powiedziałem kiedyś do mojego przyjaciela, że w górach jestem najlepszą wersją siebie.'
card_image:
    Clip.png:
        name: Clip.png
        type: image/png
        size: 172082
        path: Clip.png
---

