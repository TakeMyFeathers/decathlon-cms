---
title: 'Strona główna'
menu: 'Strona główna'
anchor: home
hero_heading: 'Ruszaj z nami w góry'
hero_description: 'Wyrusz z nami na górski szlak i odkryj nieznane Ci dotąd zakątki Tatr. Rozkochaj się w pięknie dzikiej natury i przeżyj z nami górską przygodę. Zabierzemy Cię w miejsca, do których docierają tylko prawdziwi miłośnicy wędrówek. Sprawdź nasze inspiracje i czerp satysfakcję z wypraw górskich!'
heading: 'Ruszaj z nami w góry'
description: !!binary V3lydXN6IHogbmFtaSBuYSBnw7Nyc2tpIHN6bGFrIGkgb2RrcnlqIG5pZXpuYW5lIENpIGRvdMSFZCB6YWvEhXRraSBUYXRyLiBSb3prb2NoYWogc2nEmSB3IHBpxJlrbmllIGR6aWtpZWogbmF0dXJ5IGkgcHJ6ZcW8eWogeiBuYW1pIGfDs3Jza8SFIHByenlnb2TEmS4gWmFiaWVyemVteSBDacSZIAN3IG1pZWpzY2EsIGRvIGt0w7NyeWNoIGRvY2llcmFqxIUgdHlsa28gcHJhd2R6aXdpIG1pxYJvxZtuaWN5IHfEmWRyw7N3ZWsuA1NwcmF3ZMW6IG5hc3plIGluc3BpcmFjamUgaSBjemVycCBzYXR5c2Zha2NqxJkgeiB3eXByYXcgZ8OzcnNraWNoIQ==
---

