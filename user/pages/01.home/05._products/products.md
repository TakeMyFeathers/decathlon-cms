---
title: 'Homepage products'
menu: Sprzęt
anchor: products
section_title: 'Solidny sprzęt'
section_subtitle: 'Który na pewno przyda Ci się na wyprawach z nami. '
slides:
    -
        type: 'Buty Trekingowe'
        name: 'Forclaz chaussere trek 700 f'
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.'
        button_text: Sprawdź
        photo:
            product-1.png:
                name: product-1.png
                type: image/png
                size: 89260
                path: product-1.png
    -
        type: 'Buty Trekingowe'
        name: 'Forclaz chaussere trek 700 f'
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.'
        button_text: Sprawdź
        photo:
            product-2.png:
                name: product-2.png
                type: image/png
                size: 49903
                path: product-2.png
    -
        type: 'Buty Trekingowe'
        name: 'Forclaz chaussere trek 700 f'
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.'
        button_text: Sprawdź
        photo:
            product-3.png:
                name: product-3.png
                type: image/png
                size: 68029
                path: product-3.png
    -
        type: 'Buty Trekingowe'
        name: 'Forclaz chaussere trek 700 f'
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.'
        button_text: Sprawdź
        photo:
            product-4.png:
                name: product-4.png
                type: image/png
                size: 51006
                path: product-4.png
    -
        type: 'Buty Trekingowe'
        name: 'Forclaz chaussere trek 700 f'
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.'
        button_text: Sprawdź
        photo:
            product-5.png:
                name: product-5.png
                type: image/png
                size: 71133
                path: product-5.png
---

