const people = new Swiper('.s1', {
    direction: 'horizontal',
    slidesPerView: 1,
    loop: true,
    breakpoints: {
        1450: {
            slidesPerView: 2,
        },
    },
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev',
    },
});

const products = new Swiper('.s2', {
    direction: 'horizontal',
    slidesPerView: 1,
    centeredSlides: true,
    loop: true,
    spaceBetween: 20,
    speed: 500,
    breakpoints: {
        1400: {
            slidesPerView: 5,
        },
        815: {
            slidesPerView: 3,
        },
    },
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev',
    },
});
