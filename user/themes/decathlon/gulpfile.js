const { src, dest, task, watch, series } = require('gulp');
const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const purgecss = require('gulp-purgecss');
const concat = require('gulp-concat');
const del = require('del');
const logSymbols = require('log-symbols');

/** Styles */
const tailwindTask = () => {
  return src(`./css/*.css`)
    .pipe(postcss([tailwindcss('./tailwind.config.js'), autoprefixer]))
    .pipe(
      concat({
        path: 'main.css',
      })
    )
    .pipe(
      purgecss({
        content: ['./templates/**/*.twig', './css/*.css'],
        defaultExtractor: (content) => {
          const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || [];
          const innerMatches =
            content.match(/[^<>"'`\s.()]*[^<>"'`\s.():]/g) || [];
          return broadMatches.concat(innerMatches);
        },
      })
    )
    .pipe(dest('./compiled-css'));
};

/** Watching for changes  */
const watchFiles = () => {
  watch(
    [`./templates/**/*.twig`, `./css/*.css`, `./js/*.js`],
    tailwindTask
  );
  console.log('Watching for Changes..\n');
};

const cleanTask = () => {
  console.log(
    '\n\t' + logSymbols.info,
    'Cleaning dist folder for fresh start.\n'
  );
  return del(['./compiled-css']);
};

/** Gulp tasks */

exports.default = series(
  cleanTask,
  tailwindTask,
  watchFiles
);

exports.build = series(
  cleanTask,
  tailwindTask
);
