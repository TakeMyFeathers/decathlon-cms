module.exports = {
    mode: 'jit',
    content: ['./templates/**/*.{html,twig}'],
    theme: {
        screens: {
            xl: {max: '1279px'},
            lg: {max: '1024px'},
            md: {max: '815px'},
            sm: {max: '560px'},
            xs: {max: '430px'},
            'landscape-md': {'raw': '(orientation: landscape) and (max-height: 815px)'},
            'landscape-sm': {'raw': '(orientation: landscape) and (max-height: 560px)'},
        },
        extend: {
            colors: {
                'light-blue': '#0F8BCB',
                'dark-blue': '#084D75',
                dark: '#042F4B',
            },
            backgroundImage: {
                'home-background': "url('../images/backgrounds/hero-background.png')",
                'about-background': "url('../images/backgrounds/about-background.png')",
                'routes-background': "url('../images/backgrounds/routes-background.png')",
                'products-background': "url('../images/backgrounds/products-background.png')",
                'blog-background': "url('../images/backgrounds/blog-background.png')",
            },
            fontFamily: {
                roboto: "'Roboto Condensed'",
                geomanist: ['Geomanist'],
            },
            padding: {
                'pt-17': '4.5rem',
            },
            keyframes: {
                'bounce-short': {
                    '0%': {
                        transform: 'translateY(-25%)',
                        'animation-timing-function': 'cubic-bezier(0.8, 0, 1, 1)',
                    },
                    '50%': {
                        transform: 'translateY(0)',
                        'animation-timing-function': 'cubic-bezier(0, 0, 0.2, 1)',
                    },
                    '75%': {
                        transform: 'translateY(-15%)',
                        'animation-timing-function': 'cubic-bezier(0.8, 0, 1, 1)',
                    },
                    '100%': {
                        transform: 'translateY(0)',
                        'animation-timing-function': 'cubic-bezier(0, 0, 0.2, 1)',
                    },
                },
            },
            animation: {
                'bounce-short': 'bounce-short 1s 1',
            },
            zIndex: {
                1: '1',
                always: '9999',
            },
            skew: {
                10: '10deg',
            }
        },
    },
    plugins: [],
}
